import { VitroApiService } from "./vitro-api.service";
require('dotenv').config()


const patients = require(process.env.PATIENTS_FILE_PATH!);


const login = async () => {
  const api = new VitroApiService({url: process.env.VITRO_URL, user: process.env.VITRO_USER, password: process.env.VITRO_PASSWORD})
  await api.login();
  return api;
}


const start = async () => {
  //console.log(new Date());
  const api = await login();
  const embryos = [];

   while (true){

    console.log('Start',new Date());
    const promises = []

    for(const patient of patients) {
      promises.push(api.getEmbryoIDs(patient.patientIDx, patient.treatmentName))
    }

    const res = await Promise.all(promises)
    const validResponse = res.filter(e => e!==undefined)
    console.log(`Requests ${patients.length}`);
    console.log(`Valid response ${validResponse.length}`);
    console.log('End',new Date());
  }
}

start().then(_ =>
  console.log('Done')
).catch(e => {console.log(e)})


