
import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import rateLimit from 'axios-rate-limit';
import * as https from 'https';
import { VitroApi } from "./vitro-api";
const agent = new https.Agent({ rejectUnauthorized: false });
export class VitroApiService {
  private readonly path: string|undefined;
  private token: string = '';
  private readonly http;

  constructor(
    private readonly vitroApi: VitroApi,
  ) {
    this.path = vitroApi.url;

    console.log('************************** settings ***************************');
    console.log(`url: ${vitroApi.url}, user: ${vitroApi.user}, password: ${vitroApi.password}`);
    console.log(`maxRequests: ${process.env.MAX_REQUESTS}, perMilliseconds: ${process.env.PER_MILLISECONDS}`);
    console.log('***************************************************************');

    // @ts-ignore
    this.http = rateLimit(axios.create({httpsAgent: agent}), { maxRequests: +process.env.MAX_REQUESTS, perMilliseconds: +process.env.PER_MILLISECONDS })

  }

  async login(): Promise<string> {
    const axiosResponse: AxiosResponse<{ Token: string }> = await this.get(`${this.path}/LOGIN`, {
      params: { username: this.vitroApi.user, password: this.vitroApi.password },
    });

    this.token = axiosResponse.data.Token;
    console.log(this.token);
    return this.token;
  }

  async getEmbryoIDs(patientIDx: string, treatmentName: string): Promise<any> {
    const axiosResponse: AxiosResponse<string> = await this.get(`${this.path}/GET/EMBRYOID`, {
      params: { patientIDx, treatmentName },
      headers: { 'API-token': this.token },
    });
    return axiosResponse.data;
  }

  private async get(url: string, config?: AxiosRequestConfig) {
    console.log(url, config?.params);

    const res = await  this.http.get(url, {
      ...config,}).catch(e => {
      console.error('******************REQUEST ERROR*********************');
      console.error(e.code);
      console.error(e.toString());
      console.error(e);
      console.error('*****************************************************')

      return e
    })

    return res;
  }
}
