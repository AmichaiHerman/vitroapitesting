Step 1: Download Node.js Installer  
Step 2: Install Node.js and NPM from Browser  
Step 3: Type npm i
Step 4: Fill out the .env file - you need to fill in a valid customer env details (for such details you can contact Fairtility)
Step 5: Fill out the patients.json file, in order to reproduce - you need to run it on an env with at least 10 patients
Step 6: Type npm run dev:watch